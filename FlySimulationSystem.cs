using UnityEngine;

public class FlySimulationSystem : MonoBehaviour
{
    public GameObject startPosition; // object representing point P0
    public GameObject target; // object representing point P2
    public GameObject flyingObject; // object representing point B
    private GameObject supportingPoint; // object representing point Q1

    public float defaultSpeed = 1; // default speed value
    private const float speedIncreaseFactor = 1.2f; // multiplier to increase speed
    private const float speedDecreaseFactor = 0.8f; // multiplier to decrease speed
    [Range(0.01f, 1.0f)]
    public float positionFactor; // multiplier for adjusting the starting position for point P1 (along the X axis) 
    [Range(0.01f, 1.0f)]
    public float heightFactor; // multiplier for adjusting the starting position for point P1 (along the Y axis) 

    private bool isFlightPossible; // simulation capability state

    void Start()
    {
        // Setting initial coordinates for points P0 and P2
        startPosition.transform.position = new Vector3(startPosition.transform.position.x, startPosition.transform.position.y, 0);
        target.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, 0);

        // exception processing when the points P0 and P2 have the same coordinates along the X axis
        if (startPosition.transform.position.x == target.transform.position.x)
        {
            Debug.LogError("The target and the starting point cannot have the same X coordinates");
            isFlightPossible = false;
            return;
        }
        else isFlightPossible = true;

        // calculation of distance between points P0 and P2
        float startDistance = Vector3.Distance(startPosition.transform.position, target.transform.position);

        // coordinate calculation for point Q1
        float supportingPointPosition_X = startPosition.transform.position.x < target.transform.position.x ?
        startPosition.transform.position.x + startDistance * positionFactor :
        startPosition.transform.position.x - startDistance * positionFactor;
        float supportingPointPosition_Y = startPosition.transform.position.y + startDistance * heightFactor;

        // initial positioning for points Q1 and B
        supportingPoint = new GameObject();
        supportingPoint.transform.position = new Vector3(supportingPointPosition_X, supportingPointPosition_Y, 0);
        flyingObject.transform.position = startPosition.transform.position;
    }

    void Update()
    {
        if (isFlightPossible)
        {
            // calculation of distance between points P0 and P2
            float distanceOverall = Vector3.Distance(startPosition.transform.position, target.transform.position);
            // calculation of distance between points P0 and B
            float distanceCovered = Vector3.Distance(startPosition.transform.position, flyingObject.transform.position);
            // calculation of distance between points P2 and B
            float distanceLeft = Vector3.Distance(flyingObject.transform.position, target.transform.position);

            if ((distanceOverall > distanceCovered + distanceLeft) || (distanceOverall < Mathf.Abs(distanceCovered - distanceLeft)))
            {
                // exceptions processing
                isFlightPossible = false;
                Debug.LogError("No solutions for given coordinates");
            }
            else if ((distanceOverall == 0) && (distanceCovered == distanceLeft))
            {
                // exceptions processing
                isFlightPossible = false;
                Debug.LogError("Infinite number of solutions for given coordinates");
            }
            else
            {
                // calculation of the distance between point P0 and the projection of point B
                float distanceToProjection = (distanceCovered * distanceCovered - distanceLeft * distanceLeft +
                distanceOverall * distanceOverall) / (2 * distanceOverall);

                // additional functionality (can be used to display the projection of point B)
                // calculation of coordinates for the projection of point B
                float heightToProjection = Mathf.Sqrt(distanceCovered * distanceCovered -
                distanceToProjection * distanceToProjection);
                float projection_X = startPosition.transform.position.x + distanceToProjection *
                (target.transform.position.x - startPosition.transform.position.x) / distanceOverall;
                float projection_Y = startPosition.transform.position.y + distanceToProjection *
                (target.transform.position.y - startPosition.transform.position.y) / distanceOverall;

                if (distanceToProjection > distanceOverall)
                {
                    // processing of the incorrect trajectory 
                    flyingObject.transform.position = target.transform.position;
                    isFlightPossible = false;
                    Debug.LogError("Incorrect trajectory reached");
                }
                else
                {
                    // rotation of a flying object
                    Vector3 relativePos = (supportingPoint.transform.position - flyingObject.transform.position);
                    Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
                    flyingObject.transform.rotation = rotation;

                    // current speed calculation
                    float speed = flyingObject.transform.position.y <= supportingPoint.transform.position.y ?
                    defaultSpeed * speedDecreaseFactor : defaultSpeed * speedIncreaseFactor;

                    // distance to be covered in one frame
                    float maxDistanceDelta = speed * Time.deltaTime;

                    // movement of points Q1 and B
                    supportingPoint.transform.position = Vector3.MoveTowards(supportingPoint.transform.position,
                    target.transform.position, maxDistanceDelta);
                    flyingObject.transform.position = Vector3.MoveTowards(flyingObject.transform.position,
                    supportingPoint.transform.position, maxDistanceDelta);

                    // processing when a flying object reaches the coordinates of the target
                    if (flyingObject.transform.position == target.transform.position)
                    {
                        // exceptions processing
                        isFlightPossible = false;
                        Debug.Log("Flying object reached destination");
                    }
                }
            }
        }
    }
}
